import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase/note_list.dart';
import 'package:flutter/material.dart';

class AddNote extends StatefulWidget {
  const AddNote({Key? key}) : super(key: key);

  @override
  State<AddNote> createState() => _AddNoteState();
}

class _AddNoteState extends State<AddNote> {
  @override
  Widget build(BuildContext context) {
    TextEditingController noteController = TextEditingController();
    TextEditingController descriptionController = TextEditingController();
    TextEditingController locationController = TextEditingController();

    Future _addNotes() {
      final note = noteController.text;
      final description = descriptionController.text;
      final location = locationController.text;

      final ref = FirebaseFirestore.instance.collection("notes").doc();

      return ref
          .set({
            "note_name": note,
            "description": description,
            "location": location,
            "doc_id": ref.id,
          })
          .then((value) => log("collection added"))
          .catchError((onError) => log("error"));
    }

    return Column(
      children: [
        Column(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
              child: TextField(
                controller: noteController,
                decoration: InputDecoration(
                  icon: const Icon(Icons.note_add),
                  hintText: "Enter Note",
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
              child: TextField(
                controller: descriptionController,
                decoration: InputDecoration(
                  icon: const Icon(Icons.notes),
                  hintText: "Enter A Note Description",
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
              child: TextField(
                controller: locationController,
                decoration: InputDecoration(
                  icon: const Icon(Icons.add_home),
                  hintText: "Add Location",
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 15,
            ),
            ElevatedButton(
              onPressed: () {
                _addNotes();
              },
              child: const Text("Save Note"),
            ),
          ],
        ),
        Container(
          height: 400,
          child: ListView(
            children: [NoteList()],
          ),
        ),
      ],
    );
  }
}
