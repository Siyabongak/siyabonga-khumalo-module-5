import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';

class NoteList extends StatefulWidget {
  const NoteList({Key? key}) : super(key: key);

  @override
  State<NoteList> createState() => _NoteListState();
}

class _NoteListState extends State<NoteList> {
  final Stream<QuerySnapshot> _myNotes =
      FirebaseFirestore.instance.collection("notes").snapshots();

  @override
  Widget build(BuildContext context) {
    TextEditingController _noteFieldController = TextEditingController();
    TextEditingController _descriptionFieldController = TextEditingController();
    TextEditingController _locationFieldController = TextEditingController();

    void _delete(docId) {
      FirebaseFirestore.instance
          .collection("notes")
          .doc(docId)
          .delete()
          .then((value) => print("deleted"));
    }

    void _update(data) {
      var collection = FirebaseFirestore.instance.collection("notes");
      _noteFieldController.text = data["note_name"];
      _descriptionFieldController.text = data["description"];
      _locationFieldController.text = data["location"];

      showDialog(
          context: context,
          builder: (_) => AlertDialog(
                title: Text("Update"),
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    TextField(
                      controller: _noteFieldController,
                    ),
                    TextField(
                      controller: _descriptionFieldController,
                    ),
                    TextField(
                      controller: _locationFieldController,
                    ),
                    TextButton(
                      onPressed: () {
                        collection.doc(data["doc_id"]).update({
                          "note_name": _noteFieldController.text,
                          "description": _descriptionFieldController.text,
                          "location": _locationFieldController.text,
                        });
                        Navigator.pop(context);
                      },
                      child: Text("Update"),
                    ),
                  ],
                ),
              ));
    }

    return StreamBuilder(
      stream: _myNotes,
      builder: (BuildContext context,
          AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return CircularProgressIndicator();
        }

        if (snapshot.hasData) {
          return Row(
            children: [
              Expanded(
                child: SizedBox(
                  height: (MediaQuery.of(context).size.height),
                  width: MediaQuery.of(context).size.width,
                  child: ListView(
                      children: snapshot.data!.docs
                          .map((DocumentSnapshot documentSnapshot) {
                    Map<String, dynamic> data =
                        documentSnapshot.data()! as Map<String, dynamic>;
                    return Column(
                      children: [
                        Card(
                          child: Column(
                            children: [
                              ListTile(
                                title: Text(data['note_name']),
                                subtitle: Text(data['description']),
                                trailing: Text(data['location']),
                              ),
                              ButtonTheme(
                                child: ButtonBar(
                                  children: [
                                    OutlinedButton(
                                      onPressed: () {
                                        _update(data);
                                      },
                                      child: Icon(Icons.edit),
                                    ),
                                    OutlinedButton(
                                      onPressed: () {
                                        _delete(data["doc_id"]);
                                      },
                                      child: Icon(Icons.delete),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    );
                  }).toList()),
                ),
              ),
            ],
          );
        } else {
          return (Text("No data"));
        }
      },
    );
  }
}
